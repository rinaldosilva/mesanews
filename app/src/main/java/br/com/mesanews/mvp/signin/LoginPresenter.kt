package br.com.mesanews.mvp.signin

import br.com.mesanews.Utils
import br.com.mesanews.mvp.model.User

class LoginPresenter(val view: LoginContract.View) : LoginContract.Presenter {
    lateinit var model : LoginModel
    override fun validateSignin(user:User) {
        validate(user)
    }

    private fun validate(user:User) {
        if (!Utils.isValidEmail(user.email)) {
            view.showEmailError()
            return
        }
        if (user.password.isEmpty() || user.password.length < 6) {
            view.showPasswordError()
            return
        }
        model = LoginModel(this)
        model.signin(user)
    }

    override fun result(token: String) {
        if(token.isNotEmpty()) {
            view.saveToken(token)
            view.gotoFeed()
        }else {
            view.showError("")
        }
    }

    override fun showError(message: String) {
        view.showError(message)
    }
}