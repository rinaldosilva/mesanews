/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import br.com.mesanews.mvp.model.News
import br.com.mesanews.mvp.model.User
import br.com.mesanews.network.Connection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedModel(val presenter: FeedContract.Presenter) : FeedContract.Model {
    lateinit var connection: Connection

    override fun getCarrossel(token: String) {
        connection = Connection()

        val call = connection.carrossel(token)
        call.enqueue(object : Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {
                presenter.resultCarrossel(response.body() as News)
            }

            override fun onFailure(call: Call<News>, t: Throwable) {
                presenter.showError(t.message.toString())
            }
        })
    }

    override fun getNews(published_at: String, current_page: String, per_page: String, token: String) {
        connection = Connection()

        val call = connection.news(published_at,current_page,per_page,token)

        call.enqueue(object : Callback<News> {
            override fun onResponse(call: Call<News>, response: Response<News>) {
                presenter.resultNews(response.body() as News)
            }

            override fun onFailure(call: Call<News>, t: Throwable) {
                presenter.showError(t.message.toString())
            }
        })
    }

}