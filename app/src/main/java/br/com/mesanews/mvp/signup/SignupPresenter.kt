/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.signup

import br.com.mesanews.mvp.model.User

class SignupPresenter(val view: SignupContract.View) : SignupContract.Presenter {
    lateinit var model : SignupModel
    override fun validateSignup(user:User) {
        validate(user)
    }

    private fun validate(user:User) {
        if (user.email.isEmpty()) {
            return
        }
        if (user.password.isEmpty()) {
            return
        }
        model = SignupModel(this)
        model.signup(user)
    }

    override fun result(token: String) {
        if(token.isNotEmpty())
            view.gotoFeed()
        else {
            view.showError("")
        }
    }

    override fun showError(message: String) {
        view.showError(message)
    }
}