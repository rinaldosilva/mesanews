package br.com.mesanews.mvp.feed

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.mesanews.R
import br.com.mesanews.databinding.ActivityFeedBinding
import br.com.mesanews.mvp.model.News

class FeedActivity : AppCompatActivity(), FeedContract.View {

    lateinit var feedPresenter: FeedPresenter
    lateinit var binding: ActivityFeedBinding
    lateinit var feedAdapter: RecyclerView.Adapter<*>
    lateinit var highlightAdapter: RecyclerView.Adapter<*>
    lateinit var feedLayoutManager: RecyclerView.LayoutManager
    lateinit var highLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.getBooleanExtra("isFromSignup",false))
            Toast.makeText(this,getString(R.string.msg_bemvindo),Toast.LENGTH_SHORT).show()

        binding = ActivityFeedBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loading.visibility = View.VISIBLE

        feedPresenter = FeedPresenter(this)

        feedPresenter.requestNews("2021-02-27", current_page = "1", per_page = "20",
            token = getToken()
        )

        feedPresenter.requestCarrossel(getToken())

    }
    fun getToken() : String {
        val sharedPref =
            getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        return sharedPref.getString("token","").toString()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.feed_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.search -> {
                openSearch()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openSearch() {
        val intent = Intent(this, SearchActivity::class.java)
        startActivityForResult (intent,321)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == 321){
                binding.loading.visibility = View.VISIBLE
                val data = intent.extras?.get("data_search").toString()
                feedPresenter.requestNews(data, current_page = "1", per_page = "20", token = getToken())
            }
        }
    }

    override fun bindCarrossel(news: News) {
        highLayoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)

        highlightAdapter = HighLightsAdapter(news, this)

        binding.recyclerHighlights.apply {
            setHasFixedSize(true)
            layoutManager = highLayoutManager
            adapter = highlightAdapter
        }
    }

    override fun bindFeed(news: News) {
        feedLayoutManager = LinearLayoutManager(this)

        feedAdapter = FeedAdapter(news, this)

        binding.recyclerFeed.apply {
            setHasFixedSize(true)
            layoutManager = feedLayoutManager
            adapter = feedAdapter
        }
        binding.loading.visibility = View.GONE
    }

    override fun showError(msg: String) {
        TODO("Not yet implemented")
    }
}
