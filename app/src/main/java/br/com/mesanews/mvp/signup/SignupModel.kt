/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.signup

import br.com.mesanews.mvp.model.User
import br.com.mesanews.network.Connection
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignupModel(val presenter: SignupContract.Presenter) : SignupContract.Model {
    lateinit var connection: Connection
    override fun signup(user: User) {
        connection = Connection()

        val call = connection.signin(user)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                presenter.result(response.body()!!.token)
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                presenter.showError(t.message.toString())
            }
        })
    }

}