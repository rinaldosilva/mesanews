/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.signup

import br.com.mesanews.mvp.model.User

interface SignupContract{
    interface View{
        fun signup()
        fun gotoFeed()
        fun showError(msg: String)
    }

    interface Presenter{
        fun validateSignup(user:User)
        fun result(token: String)
        fun showError(message: String)
    }

    interface Model{
        fun signup(user:User)
    }
}