/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import br.com.mesanews.mvp.model.News

class FeedPresenter(val view: FeedContract.View) : FeedContract.Presenter {
    lateinit var model: FeedModel

    override fun requestCarrossel(token: String) {
        model = FeedModel(this)
        model.getCarrossel(token)
    }

    override fun requestNews(published_at: String, current_page: String, per_page: String, token: String) {
        model = FeedModel(this)
        model.getNews(published_at,current_page,per_page,token)
    }

    override fun resultNews(news: News) {
        view.bindFeed(news)
    }

    override fun resultCarrossel(news: News) {
        view.bindCarrossel(news)
    }

    override fun showError(message: String) {
        view.showError(message)
    }
}