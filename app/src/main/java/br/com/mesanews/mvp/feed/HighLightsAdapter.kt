/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.mesanews.mvp.model.News

class HighLightsAdapter(private val news: News, val context: Context) :
    RecyclerView.Adapter<HighLightsAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = NewsView(context,true)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return news.data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, pos: Int) {
        (holder.itemView as NewsView).bind(context,pos, news.data.get(pos))
        holder.setIsRecyclable(false)
        holder.itemView.setOnClickListener {
            it
            val intent = Intent(context, DetailActivity::class.java)
            intent.putExtra("item_news",news.data[pos])
            context.startActivity(intent)
        }
    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}