/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import br.com.mesanews.mvp.model.News

interface FeedContract {
    interface View {
        fun bindCarrossel(news: News)
        fun bindFeed(news: News)
        fun showError(msg: String)
    }

    interface Presenter {
        fun requestCarrossel(token: String)
        fun requestNews(published_at: String, current_page: String, per_page: String,token: String)
        fun resultNews(news: News)
        fun resultCarrossel(news: News)
        fun showError(message: String)
    }

    interface Model {
        fun getCarrossel(token: String)
        fun getNews(published_at: String, current_page: String, per_page: String, token: String)
    }
}