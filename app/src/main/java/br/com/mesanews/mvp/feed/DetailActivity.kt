/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.widget.CheckedTextView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import br.com.mesanews.databinding.ActivityFeedDetailBinding
import br.com.mesanews.mvp.model.News

class DetailActivity : AppCompatActivity() {
    lateinit var binding: ActivityFeedDetailBinding
    lateinit var itemNews: News.Data
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFeedDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadFontAwesome(binding.favorite)

        binding.favorite.setOnClickListener {
            if (binding.favorite.isChecked) {
                binding.favorite.setTextColor(Color.LTGRAY)
                binding.favorite.isChecked = false
            } else {
                binding.favorite.isChecked = true
                binding.favorite.setTextColor(Color.YELLOW)
            }
        }

        loadNews()
    }

    private fun loadNews() {
        itemNews = intent.extras?.get("item_news") as News.Data

        binding.myWebview.loadUrl(itemNews.url)
    }

    private fun loadFontAwesome(view: TextView) {
        val textView: TextView = view
        val font = Typeface.createFromAsset(this.assets, "fontawesome.ttf")
        textView.typeface = font
    }

}
