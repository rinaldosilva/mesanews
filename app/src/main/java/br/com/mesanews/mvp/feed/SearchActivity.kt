/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import br.com.mesanews.databinding.ActivityFeedSearchBinding
import br.com.mesanews.mvp.model.News

class SearchActivity : AppCompatActivity() {
    lateinit var binding: ActivityFeedSearchBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFeedSearchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnPesquisar.setOnClickListener {
            val intent = Intent()
            intent.putExtra("data_search", binding.tiDataNoticia.text.toString())
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }

}
