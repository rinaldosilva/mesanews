package br.com.mesanews.mvp.signin

import br.com.mesanews.mvp.model.User

interface LoginContract{
    interface View{
        fun signin()
        fun signup()
        fun gotoFeed()
        fun showEmailError()
        fun showPasswordError()
        fun showError(msg: String)
        fun saveToken(token: String)
    }

    interface Presenter{
        fun validateSignin(user:User)
        fun result(token: String)
        fun showError(message: String)
    }

    interface Model{
        fun signin(user:User)
    }
}