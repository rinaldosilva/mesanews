/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.model

import java.io.Serializable

data class News(
    val pagination: Pagination,
    val data: List<Data>
) {

    data class Data(
        val title: String,
        val description: String,
        val content: String,
        val author: String,
        val publishedAt: String,
        var favorite: Boolean,
        val highlight: Boolean,
        val url: String,
        val image_url: String
    ): Serializable

    data class Pagination(
        val currentPage: Long,
        val perPage: Long,
        val totalPages: Long,
        val totalItems: Long
    )
}