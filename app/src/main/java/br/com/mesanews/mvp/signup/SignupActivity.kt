/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.signup

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.mesanews.R
import br.com.mesanews.databinding.ActivitySignupBinding
import br.com.mesanews.mvp.feed.FeedActivity
import br.com.mesanews.mvp.model.User
import com.facebook.CallbackManager

class SignupActivity : AppCompatActivity(), SignupContract.View {

    lateinit var presenter: SignupPresenter
    lateinit var binding: ActivitySignupBinding
    lateinit var callbackManager: CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = SignupPresenter(this)

        callbackManager = CallbackManager.Factory.create()

        setListeners()
    }

    private fun setListeners() {
        binding.btnCadastro.setOnClickListener {
            signup()
        }
    }

    override fun signup() {
        presenter.validateSignup(
            User(
                email = binding.tiEmail.text.toString(),
                password = binding.tiSenha.text.toString(),
                name = binding.tiNome.text.toString()   ,
                token = ""
            )
        )
    }

    override fun gotoFeed() {
        val intent = Intent(this, FeedActivity::class.java)
        intent.putExtra("isFromSignup",true)
        startActivity(intent)
        finish()
    }

    override fun showError(message: String) {
        Toast.makeText(this, getString(R.string.msg_erro_login), Toast.LENGTH_SHORT).show()
    }


}
