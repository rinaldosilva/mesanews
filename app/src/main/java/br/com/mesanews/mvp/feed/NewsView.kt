/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.feed

import android.content.Context
import android.content.Intent
import android.content.res.AssetManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.CheckedTextView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import br.com.mesanews.R
import br.com.mesanews.mvp.model.News
import br.com.mesanews.mvp.signup.SignupActivity
import coil.api.load
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import java.net.URL

class NewsView(context: Context?,var isCarrossel : Boolean) : RelativeLayout(context) {
    private var favorite : CheckedTextView

    init {
        View.inflate(context, (if(isCarrossel)  R.layout.new_view_highlight else R.layout.new_view) , this)
        loadFontAwesome(findViewById(R.id.favorite))
        favorite = findViewById<View>(R.id.favorite) as CheckedTextView
    }

    fun bind(context: Context, pos: Int, item: News.Data) {
        (findViewById<View>(R.id.title) as TextView).setText(item.title)
        (findViewById<View>(R.id.description) as TextView).setText(item.description)

        (findViewById<View>(R.id.image) as ImageView).load(item?.image_url){
            placeholder(R.drawable.ic_launcher_background)

        }

        favorite.setOnClickListener {
            if(item.favorite){
                favorite.setTextColor(Color.LTGRAY)
                item.favorite = false
            }else{
                item.favorite = true
                favorite.setTextColor(Color.YELLOW)
            }
        }
    }
    private fun loadFontAwesome(view: TextView) {
        val textView: TextView = view
        val font = Typeface.createFromAsset(context.assets, "fontawesome.ttf")
        textView.typeface = font
    }

}