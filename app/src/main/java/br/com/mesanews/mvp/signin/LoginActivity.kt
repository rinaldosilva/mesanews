/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.mvp.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import br.com.mesanews.R
import br.com.mesanews.databinding.ActivityLoginBinding
import br.com.mesanews.mvp.feed.FeedActivity
import br.com.mesanews.mvp.model.User
import br.com.mesanews.mvp.signup.SignupActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult

class LoginActivity : AppCompatActivity(), LoginContract.View {

    lateinit var presenter: LoginPresenter
    lateinit var binding: ActivityLoginBinding
    lateinit var callbackManager: CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = LoginPresenter(this)

        callbackManager = CallbackManager.Factory.create()

        setListeners()
    }

    private fun setListeners() {
        //Facebook auth.
        //binding.btnFacebook.registerCallback(callbackManager,this)
        binding.btnFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    gotoFeed()
                }

                override fun onCancel() {

                }

                override fun onError(error: FacebookException?) {
                    showError(error?.message.toString())
                }
            })

        //Email and password atuh.
        binding.btnEntrar.setOnClickListener {
            signin()
        }

        binding.btnSignup.setOnClickListener {
            signup()
        }
    }

    override fun signin() {
        presenter.validateSignin(
            User(
                email = binding.tiEmail.text.toString(),
                password = binding.tiSenha.text.toString(),
                name = "",
                token = ""
            )
        )
    }

    override fun signup() {
        val intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
    }

    override fun gotoFeed() {
        val intent = Intent(this, FeedActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun showEmailError() {
        binding.tiEmail.setError(getString(R.string.msg_login_error))
    }

    override fun showPasswordError() {
        binding.tiSenha.setError(getString(R.string.msg_senha_error))
    }

    override fun showError(message: String) {
        Toast.makeText(this, getString(R.string.msg_erro_login), Toast.LENGTH_SHORT).show()
    }

    override fun saveToken(token: String) {
        val sharedPref =
            getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        sharedPref.edit().putString("token", token)
            .commit()
    }

}
