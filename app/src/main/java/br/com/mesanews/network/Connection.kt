/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.network

import br.com.mesanews.mvp.model.News
import br.com.mesanews.mvp.model.User
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Connection {
    var URL_BASE = "https://run.mocky.io/"
    lateinit var restConnection: RestConnection
    lateinit var client : OkHttpClient
    lateinit var retrofit: Retrofit

    init {
        config()
    }
    private fun config(){
        client = OkHttpClient().newBuilder().build()

        retrofit = Retrofit.Builder()
            .baseUrl(URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        restConnection = retrofit.create(RestConnection::class.java)
    }

    fun signin(user: User): Call<User> {
        return restConnection.signin(user,"application/json")
    }

    fun signup(user: User): Call<User> {
        return restConnection.signup(user,"application/json")
    }

    fun news(published_at: String,current_page:String,per_page:String,token: String): Call<News>{
        return restConnection.news(current_page,per_page,published_at,"application/json",token)
    }
    fun carrossel(token:String): Call<News> {
        return restConnection.carrossel("application/json",token)
    }
}