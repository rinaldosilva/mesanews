/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews.network

import br.com.mesanews.mvp.model.News
import br.com.mesanews.mvp.model.User
import retrofit2.Call
import retrofit2.http.*

interface RestConnection {
    @POST("v3/f4fa47d8-bdbd-4caf-a16e-0baa3a734e9c")
    fun signin(@Body user: User, @Header("Content-Type") content_type: String): Call<User>

    @POST("v3/f4fa47d8-bdbd-4caf-a16e-0baa3a734e9c")
    fun signup(@Body user: User, @Header("Content-Type") content_type: String): Call<User>

    @GET("v3/cf699ec1-c8be-4be4-a076-aa733c4b1ade")
    fun news(
        @Query("current_page") current_page: String,
        @Query("per_page") per_page: String,
        @Query("current_page") published_at: String,
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String
    ): Call<News>

    @GET("v3/c3acecd4-fb35-4e42-8d98-47880f6d473c")
    fun carrossel(
        @Header("Content-Type") content_type: String,
        @Header("Authorization") authorization: String
    ): Call<News>
}