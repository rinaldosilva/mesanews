/*
 * Copyright (c)  Criado por Rinaldo Silva
 */

package br.com.mesanews

import java.util.regex.Pattern

object Utils {
    @JvmStatic
    fun isValidEmail(email: String) : Boolean{
        val regex = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")

        var pattern = Pattern.compile(regex,Pattern.CASE_INSENSITIVE)
        var matcher = pattern.matcher(email)
        return matcher.matches()

    }
}